/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;

import countrydemo.Communication;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 *
 * @author Kate
 */
public class CountryDemo {

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {
        Communication communication = new Communication();
      //  communication.ReadingFromFile();
        System.out.println("Вас приветствует программа страны. Хотите продолжить работу с программой? yes/no");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine();
        if (input.equals("yes") | input.equals("y")) {

            communication.readingFromFile();
            try {

                System.out.println("Введите страну: ");
                String line = reader.readLine();
                while (!line.isEmpty()) {
                    System.out.println("Введите численность населения: ");
                    String number = reader.readLine();
                    Double numberPopulation = 0.0;
                    if (!number.isEmpty()) {
                        numberPopulation = Double.parseDouble(number);
                    }
                    communication.setMapCountry(line, numberPopulation);
                    System.out.println("Введите страну: ");
                    line = reader.readLine();
                }
            } catch (IOException ex) {

            }
        
            System.out.println("Сохранить в файл? yes/no");
            input = reader.readLine();
            if (input.equals("yes") | input.equals("y")) {
                communication.saveToFile(communication.getMapCountry());
            }
            System.out.println("Вывести на экран информацию о странах? yes/no");
            input = reader.readLine();
            if (input.equals("yes") | input.equals("y")) {
                System.out.println("Вывод на экран информации о странах: ");
            communication.getMapCountry().entrySet().stream().forEach((pair) -> {
                System.out.println("Страна: " + pair.getKey() + "\t численность населения : " + pair.getValue() + "\n");
            });
            }
            System.out.println("Поиск по странам? yes/no");
            input = reader.readLine();
            if (input.equals("yes") | input.equals("y")) {
                System.out.println("Введите страну, которую хотите найти: ");
                String country = reader.readLine();
                System.out.println(communication.searchByCountry(country));
                System.out.println("Завершение программы.");
            } else {
                System.out.println("Завершение программы.");
            }
        
        }
    }
    
}
