/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package countrydemo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kate
 */
public class Communication extends OperationWithFile {

  /*  public Communication(String countryInput, Double populationInput) {
        super(countryInput, populationInput);
    }*/
    
    private Map<String, Double> mapState = new HashMap<>();

  /*  public Communication() {
        super();
    }*/
   /* boolean checkTheFileOnTheVoid() throws FileNotFoundException { 
        FileInputStream fileInputStream = null;
        boolean flag = true;
        try {
            fileInputStream = new FileInputStream(fileName);
            long sizeFile = fileInputStream.getChannel().size();
            if (sizeFile != 0) {
                flag = false;
            }
        } catch (Exception ex) {
            Logger.getLogger(Communication.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileInputStream.close();
            } catch (IOException ex) {
                    
            }
        }
        return flag;
    }*/
    
    public void readingFromFile() throws FileNotFoundException, Exception {
      /*  boolean flag = checkTheFileOnTheVoid();
        if (!flag) {
            mapState = readFromFile();
        }*/
        mapState = readFromFile();
    }
   /* public void InputKeyboard (String country, Double population) {
        mapState.put(country, population);
    } 
    */
    public Map<String, Double> getMapCountry() {
        return mapState;
    }
    public void setMapCountry(String country, Double population ) {
        mapState.put(country, population);    
    }
    
    public String searchByCountry(String country) {
        for(Entry<String, Double> pair: mapState.entrySet()) {
            if (country.equalsIgnoreCase(pair.getKey())) {
                //  info = String.join(pair.getKey(), "\t", Double.parseDouble(pair.getValue()));
                return pair.getKey() + "\t" + pair.getValue();
                
            }
        }
        return "Country not found";
    }
}
