/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package countrydemo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author Kate
 */
public abstract class OperationWithFile {

    final File fileName = new File("D:/eclipsec/CountryDemo/mapCountry.txt");
   // final String fileName = ;
  /*  public OperationWithFile() {
      //  this.country = null;
      //  this.population = 0.0;
    }*/
  /*  public OperationWithFile(String countryInput, Double populationInput) {
        this.country = countryInput;
        this.population = populationInput;
    }*/
    
    /**
     *
     * @param mapOfCountries
     * 
     */
    public void saveToFile(Map<String, Double> mapOfCountries) {
      /*  OutputStream outStream = new FileOutputStream("D:/eclipsec/CountryDemo/mapCountry.txt");
        */
       // Writer writer = null;
        PrintWriter writer = null;
        try {
           // writer = new FileWriter("D:/eclipsec/CountryDemo/mapCountry.txt");
          //  writer = new FileWriter(fileName);
            writer = new PrintWriter(new FileWriter(fileName),true);
         //   writer = new PrintWriter(fileName);
            for (Map.Entry<String, Double> pair : mapOfCountries.entrySet()) {
                //writer.write(pair.getKey() + "\t");
                writer.print(pair.getKey() + "\t");
               // writer.write(Double.toString(pair.getValue())+ "\r\n");// всй равно пришдлсь добавить "\r\n", чтобы файл нормально записался
                 writer.println(Double.toString(pair.getValue()));
            }
    //        writer.flush();// автоматической очистки буфера вкл выше 
        } catch (Exception e) {
           // Logger.getLogger(ListToFile.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (Exception ex) {
                    
                }
            }
        }
        
    }
    Map<String, Double> readFromFile() throws Exception{
       // Scanner scanner = new Scanner(new File("D:/eclipsec/CountryDemo/mapCountry.txt"));
        Scanner scanner = null;
        FileInputStream fileInputStream = null;
        Map<String, Double> mapCountry = new HashMap<>();
        try {
            scanner = new Scanner(fileName, "UTF-8");
            fileInputStream = new FileInputStream(fileName);
            long sizeFile = fileInputStream.getChannel().size();
            if (sizeFile != 0) {
            while (scanner.hasNextLine()) {
                String s = scanner.nextLine(); //считали всю строку в файле
                String[] array = s.split("\t");
                Double value = Double.valueOf(array[array.length-1]);
                String countryKey = "";
                for ( int i = 0; i < array.length-1; ++i ) {
                    countryKey += array[i];
                }
               // String country = s.substring( 0, s.indexOf( '\t' ) ); //!!!
                mapCountry.put(countryKey,value );
            }
            }
        } catch (FileNotFoundException | NumberFormatException e) {
            
        } finally {
            if (scanner != null) {
                try {
                    scanner.close();
                    fileInputStream.close();
                } catch (Exception ex) {
                    
                }
            }
        }
        return mapCountry;
    }
    
    
}
